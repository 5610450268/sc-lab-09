package Controller;

import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.ProfitComparator;
import Interface.Taxable;
import Model.TaxableComparator;

import java.util.ArrayList;
import java.util.Collections;

import Model.Company;
import Model.Person;
import Model.Product;

public class Tester {

	public static void main(String[] args) {
		Tester test = new Tester();
		test.testPer();
		test.testProduct();
		test.testCompany();
		test.testTax();
	}
	
	public void testPer(){
		System.out.println("Test Person");
		ArrayList<Person> person = new ArrayList<Person>();
		person.add(new Person("Pink", 1000000));
		person.add(new Person("Piku", 10000));
		person.add(new Person("Piin", 100000));
		System.out.println("---Before---");
		for(Person object : person){
			System.out.println(object);
		}
		Collections.sort(person);
		System.out.println("---After---");
		for(Person object : person){
			System.out.println(object);
		}
	}
	
	public void testProduct(){
		System.out.println("\n\nTest Product");
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("pen", 200));
		product.add(new Product("pencil", 100));
		product.add(new Product("paper", 120));
		System.out.println("---Before---");
		for(Product object : product){
			System.out.println(object);
		}
		Collections.sort(product);
		System.out.println("---After---");
		for(Product object : product){
			System.out.println(object);
		}
	}
	
	public void testCompany(){
		System.out.println("\n\nTest Company");
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Pizza", 1000000 , 800000));
		company.add(new Company("Ice-cream", 500000 , 90000));
		company.add(new Company("Yogurt", 600000 , 70000));
		System.out.println("Earning Comparator--");
		System.out.println("---Before---");
		for(Company object : company){
			System.out.println(object.getName()+":: Income="+object.getIncome());
		}
		Collections.sort(company, new EarningComparator());
		System.out.println("---After---");
		for(Company object : company){
			System.out.println(object.getName()+":: Income="+object.getIncome());
		}
		System.out.println("\n\nExpense Comparator--");
		System.out.println("---Before---");
		for(Company object : company){
			System.out.println(object.getName()+":: Expenses="+object.getExpenses());
		}
		Collections.sort(company, new ExpenseComparator());
		System.out.println("---After---");
		for(Company object : company){
			System.out.println(object.getName()+":: Expenses="+object.getExpenses());
		}
		System.out.println("\n\nProfit Comparator--");
		System.out.println("---Before---");
		for(Company object : company){
			System.out.println(object.getName()+":: Profit="+object.getProfit());
		}
		Collections.sort(company, new ProfitComparator());
		System.out.println("---After---");
		for(Company object : company){
			System.out.println(object.getName()+":: Profit="+object.getProfit());
		}
		
		
	}
	
	public void testTax(){
		System.out.println("\n\nTest Taxable");
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Person("Pink", 500000));
		tax.add(new Company("Ice-cream", 500000 , 90000));
		tax.add(new Product("Pen", 200));
		System.out.println("---Before---");
		for(Taxable obj:tax){
			System.out.println(obj.getTax());
		}
		Collections.sort(tax, new TaxableComparator());
		System.out.println("---After---");
		for(Taxable obj:tax){
			System.out.println(obj.getTax());
		}
	}
}
