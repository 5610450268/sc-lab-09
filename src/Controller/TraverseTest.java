package Controller;

import Model.InOrderTraversal;
import Model.Node;
import Model.PostOrderTraversal;
import Model.PreOrderTraversal;
import Model.ReportConsole;

public class TraverseTest {
	public static void main(String[] args){
		Node c = new Node("C", null, null);
		Node e = new Node("E", null, null);
		Node d = new Node("D", c, e);
		Node a = new Node("A", null, null);
		Node b = new Node("B", a, d);
		Node h = new Node("H", null, null);
		Node i = new Node("I", h, null);
		Node g = new Node("G", null, i);
		Node f = new Node("F", b, g);
		
		ReportConsole rep = new ReportConsole();
		System.out.print("Traversal with PreOrderTraversal : ");
		rep.display(f, new PreOrderTraversal());
		System.out.print("\nTraversal with InOrderTraversal : ");
		rep.display(f, new InOrderTraversal());
		System.out.print("\nTraversal with PostOrderTraversal : ");
		rep.display(f, new PostOrderTraversal());
	}
	
	
}
