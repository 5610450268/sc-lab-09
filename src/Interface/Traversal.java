package Interface;

import java.util.List;

import Model.Node;

public interface Traversal {
	public List<Node> traverse(Node root);
}
