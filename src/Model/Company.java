package Model;

import Interface.Taxable;

public class Company implements Taxable {
	private String name;
	private double income;
	private double expenses;
	private double profit;

	public Company(String name, double income, double expenses) {
		this.name = name;
		this.income = income;
		this.expenses = expenses;
		this.profit = income-expenses;
	}

	public String getName() {
		return this.name;
	}

	public double getIncome() {
		return this.income;
	}

	public double getExpenses() {
		return this.expenses;
	}
	
	public double getProfit() {
		return this.profit;
	}

	public String toString() {
		return name+":: Income="+income+" Expenses="+expenses+" Profit="+profit;
	}
	
	@Override
	public double getTax() {
		return (this.profit)*0.3;
	}
}
