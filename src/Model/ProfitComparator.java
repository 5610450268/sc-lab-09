package Model;

import Model.Company;
import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company x, Company y) {
		if(x.getProfit() < y.getProfit()){
			return -1 ;
		}
		else if(x.getProfit() > y.getProfit()){
			return 1 ;
		}
		return 0 ;
	}

}
