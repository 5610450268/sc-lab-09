package Model;

import Interface.Traversal;

public class Node {
	private String nodeValue;
	private Node left;
	private Node right;
	public Node(String nodeValue ,Node left, Node right){
		this.nodeValue = nodeValue;
		this.left = left;
		this.right = right;
	}
	public String getValue(){
		return this.nodeValue;
	}
	public Node getLeft(){
		return this.left;
	}
	public Node getRight(){
		return this.right;
	}
}