package Model;

import java.util.ArrayList;
import java.util.List;

import Interface.Traversal;

public class PreOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node root) {
		// TODO Auto-generated method stub
		List<Node> node = new ArrayList<Node>();
		if(root == null){
			return node;
		}
		node.add(root);
	    if(root.getLeft() != null){
	    	node.addAll(traverse(root.getLeft()));
	    }
	    if(root.getRight() != null){
	    	node.addAll(traverse(root.getRight()));
	    }
		return node;
	}
}
