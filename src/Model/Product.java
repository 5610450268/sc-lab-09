package Model;

import Interface.Taxable;

public class Product implements Comparable, Taxable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}
	
	public String toString() {
		return name+":: price="+price;
	}
	
	@Override
	public double getTax() {
		return this.price*0.07;
	}
	
	@Override
	public int compareTo(Object object) {
		Product other = (Product) object;
		if(price < other.price) {
			return -1;
		}
		if(price > other.price) {
			return 1;
		}
		return 0;
	}

}
