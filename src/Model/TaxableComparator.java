package Model;

import Interface.Taxable;

import java.util.Comparator;

public class TaxableComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable x, Taxable y) {
		if(x.getTax() < y.getTax()) {
			return -1 ;
		}
		else if(x.getTax() > y.getTax()) {
			return 1 ;
		}
		return 0 ;
	}
}