package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	public int compare(Company x, Company y) {
		if(x.getExpenses() < y.getExpenses()){
			return -1;
		}
		if(x.getExpenses() > y.getExpenses()){
			return 1;
		}
		return 0;
	}
}
