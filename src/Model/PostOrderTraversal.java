package Model;

import java.util.ArrayList;
import java.util.List;

import Interface.Traversal;

public class PostOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node root) {
		// TODO Auto-generated method stub
		List<Node> node = new ArrayList<Node>();
		if(root == null){
			return node;
		}
	    if(root.getLeft() != null){
	    	node.addAll(traverse(root.getLeft()));
	    }
	    if(root.getRight() != null){
	    	node.addAll(traverse(root.getRight()));
	    }
	    node.add(root);
		return node;
	}
}
