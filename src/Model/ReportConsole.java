package Model;

import java.util.List;

import Interface.Traversal;
import Model.Node;

public class ReportConsole {
	public static void display (Node root, Traversal traversal){
		List<Node> node = traversal.traverse(root);
		String sum = "";
		for(Node n:node){
			System.out.print(n.getValue()+" ");
		}
	}
}
