package Model;

import Model.Company;
import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company x, Company y) {
		if(x.getIncome() < y.getIncome()){
			return -1 ;
		}
		else if(x.getIncome() > y.getIncome()){
			return 1 ;
		}
		return 0 ;
	}
}