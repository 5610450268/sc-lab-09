package Model;

import Interface.Taxable;

public class Person implements Comparable, Taxable {

	private String name;
	private double income;

	public Person(String name, double income) {
		this.name = name;
		this.income = income;
	}

	public String getName() {
		return this.name;
	}

	public double getYearlyIncome() {
		return this.income;
	}
	
	public String toString() {
		return name+":: income="+income;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		if(this.income < 300001){
			return this.income*0.05 ;
		}
		return (300000*0.05)+(this.income-300000)*0.1;
	}
	
	@Override
	public int compareTo(Object object) {
		Person other = (Person) object;
		if(income < other.income) {
			return -1;
		}
		else if(income > other.income) {
			return 1;
		}
		return 0;
	}
}
